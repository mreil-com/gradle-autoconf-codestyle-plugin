package com.mreil.gradleplugins.autoconf


import com.github.tomakehurst.wiremock.junit.WireMockRule
import groovy.util.logging.Slf4j
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import static com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder.responseDefinition
import static com.github.tomakehurst.wiremock.client.WireMock.get
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options

@Slf4j
class ConfigDownloaderTest extends Specification {
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    @Rule
    WireMockRule mockServer = new WireMockRule(options().dynamicPort())

    def "doesn't download if exists"() {
        setup:
            def configFile = testProjectDir.newFile("config.xml")

        when:
            def downloader = new ConfigDownloader("http://", configFile, log)

        then:
            !downloader.download()
    }

    def "downloads if it doesn't exists"() {
        setup:
            def fileName = "config.xml"
            def fileBody = "body"
            def target = new File(testProjectDir.root, fileName)
            stubFor(get(urlEqualTo("/" + fileName))
                    .willReturn(responseDefinition().withBody(fileBody)))

        when:
            def downloader = new ConfigDownloader("http://localhost:${mockServer.port()}/${fileName}",
                    target,
                    log)
            def downloaded = downloader.download()

        then:
            downloaded
            target.exists()
            target.readLines().first() == fileBody
    }
}
