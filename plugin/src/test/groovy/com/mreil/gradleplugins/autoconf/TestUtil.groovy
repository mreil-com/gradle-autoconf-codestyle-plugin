package com.mreil.gradleplugins.autoconf

import com.mreil.gradleplugins.autoconf.codestyle.AutoconfCodestylePlugin
import com.mreil.gradleplugins.autoconf.codestyle.CodestyleExtension
import org.gradle.api.Project

class TestUtil {
    static final List<String> DEFAULT_GRADLE_VERSIONS = ["6.0"]

    static CodestyleExtension getExtensionFromProject(Project project) {
        project.extensions.getByName(CodestyleExtension.NAME) as CodestyleExtension
    }

    static CodestyleExtension applyPluginAndGetExtension(Project project) {
        project.plugins.apply(AutoconfCodestylePlugin)
        CodestyleExtension ext = getExtensionFromProject(project)
        return ext
    }
}
