package com.mreil.gradleplugins.autoconf

import groovy.util.logging.Slf4j
import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

@Slf4j
class AbstractGradleFuncTest extends Specification {

    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    @Shared
    GradleRunner gradleRunner

    def setupSingleModuleProject(String gradleVersion) {
        setupGradleProject("single-module-project", gradleVersion)
    }

    def setupMultiModuleProject(String gradleVersion) {
        setupGradleProject("multi-module_plugin-config-in-root", gradleVersion)
    }

    def setupGradleProject(String projectDir, String gradleVersion) throws IOException {
        def config = ImmutableProjectConfig.builder()
                .root("src/test/resources/" + projectDir)
                .build()
        new ProjectHelper().setupProject(testProjectDir, config)

        gradleRunner = GradleRunner.create()
                .withPluginClasspath()
                .withGradleVersion(gradleVersion)
                .withProjectDir(testProjectDir.getRoot())
    }

    protected runTask(String... tasks) {
        def result = gradleRunner.withArguments(tasks).build()

        log.warn(result.output)
    }

}
