package com.mreil.gradleplugins.autoconf.codestyle

import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import static com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder.responseDefinition
import static com.github.tomakehurst.wiremock.client.WireMock.get
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import static com.mreil.gradleplugins.autoconf.TestUtil.applyPluginAndGetExtension

class CodenarcTest extends Specification {

    @Rule
    TemporaryFolder temp = new TemporaryFolder()

    @Rule
    WireMockRule mockServer = new WireMockRule(options().dynamicPort())

    def "apply plugin"() {
        setup:
            Project project = ProjectBuilder.builder().build()
            project.plugins.apply("groovy")
            def fileBody = "body"
            def fileName = "codenarc.xml"
            def config = new File(temp.root, fileName)
            stubFor(get(urlEqualTo("/" + fileName))
                    .willReturn(responseDefinition().withBody(fileBody)))

        when:
            CodestyleExtension ext = applyPluginAndGetExtension(project)

            ext.codenarc.configFile = config
            ext.codenarc.downloadUrl = "http://localhost:${mockServer.port()}/${fileName}"
            project.evaluate()
            project.tasks.getByName("downloadCodenarcConfig").execute()

        then:
            config.exists()
    }

}
