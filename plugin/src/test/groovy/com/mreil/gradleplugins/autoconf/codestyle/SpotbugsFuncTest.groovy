package com.mreil.gradleplugins.autoconf.codestyle

import com.mreil.gradleplugins.autoconf.AbstractGradleFuncTest
import spock.lang.Unroll

import static com.mreil.gradleplugins.autoconf.TestUtil.DEFAULT_GRADLE_VERSIONS

class SpotbugsFuncTest extends AbstractGradleFuncTest {

    @Unroll
    def "Plugin configured on root project with Gradle version #version"() {
        setup:
            setupGradleProject("multi-module_plugin-config-in-root", version)

        when: "I run the spotbugs task"
            runTask("spotbugsMain", "--info", "--stacktrace")

        then: "There is a coverage report in the java submodule"
            testProjectDir.root.toPath()
                    .resolve("java/build/reports/spotbugs/main.html")
                    .toFile().exists()

        where:
            version << DEFAULT_GRADLE_VERSIONS
    }

    @Unroll
    def "Plugin configured on subproject with Gradle version #version"() {
        setup:
            setupGradleProject("multi-module_plugin-config-in-sub", version)

        when: "I run the spotbugs task"
            runTask("spotbugsMain", "--info")

        then: "There is a coverage report in the submodule"
            testProjectDir.root.toPath()
                    .resolve("java/build/reports/spotbugs/main.html")
                    .toFile().exists()

        where:
            version << DEFAULT_GRADLE_VERSIONS
    }
}
