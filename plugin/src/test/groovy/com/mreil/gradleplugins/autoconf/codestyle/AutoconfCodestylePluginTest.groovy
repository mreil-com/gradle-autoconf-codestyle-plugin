package com.mreil.gradleplugins.autoconf.codestyle

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import spock.lang.Specification

class AutoconfCodestylePluginTest extends Specification {
    def "apply plugin"() {
        setup:
            Project project = ProjectBuilder.builder().build()

        when:
            project.plugins.apply(AutoconfCodestylePlugin)

        then:
            1 == 1
    }
}
