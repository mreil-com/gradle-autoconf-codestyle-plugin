package com.mreil.gradleplugins.autoconf;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.immutables.value.Value;

import java.nio.file.Path;
import java.nio.file.Paths;

@Value.Immutable
public interface ProjectConfig {

    String getRoot();

    @SuppressFBWarnings("PATH_TRAVERSAL_IN")
    default Path getRootPath() {
        return Paths.get(getRoot());
    }
}
