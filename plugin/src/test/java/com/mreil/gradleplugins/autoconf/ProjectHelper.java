package com.mreil.gradleplugins.autoconf;

import org.apache.commons.io.FileUtils;
import org.junit.rules.TemporaryFolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

public class ProjectHelper {

    private final Logger log = LoggerFactory.getLogger(ProjectHelper.class);

    /**
     * Prepare a gradle project at a specific file system location.
     *
     * @param tmpRoot the temporary directory where the project should live
     * @param config  the project configuration
     */
    public void setupProject(TemporaryFolder tmpRoot,
                             ProjectConfig config) {
        log.warn("Setting up project at: " + tmpRoot.getRoot());

        Path absRoot = config.getRootPath().toAbsolutePath();
        log.warn("Copying contents of: " + absRoot);

        try {
            FileUtils.copyDirectory(absRoot.toFile(), tmpRoot.getRoot());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        log.warn("Contents of project dir:");
        Arrays.stream(tmpRoot.getRoot().list()).forEach(
                log::warn
        );
    }
}
