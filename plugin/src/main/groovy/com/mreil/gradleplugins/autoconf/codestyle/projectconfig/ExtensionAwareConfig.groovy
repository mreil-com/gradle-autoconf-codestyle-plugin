package com.mreil.gradleplugins.autoconf.codestyle.projectconfig

import com.mreil.gradleplugins.autoconf.codestyle.CodestyleExtension
import org.gradle.api.Project

abstract class ExtensionAwareConfig {
    abstract void apply(Project project);

    protected static CodestyleExtension getExtension(Project project) {
        // get the extension configured on the project or its parent
        return Optional.ofNullable(getExtensionForProject(project))
                .orElseGet { -> getExtensionForProject(project.parent) }
    }

    protected static CodestyleExtension getExtensionForProject(Project project) {
        return project.extensions.findByName(CodestyleExtension.NAME) as
                CodestyleExtension
    }

}
