package com.mreil.gradleplugins.autoconf

import org.gradle.api.Project

class ProjectHelper {
    static boolean hasGroovyPlugin(Project project) {
        project.plugins.hasPlugin("groovy")
    }

    static boolean hasJavaPlugin(Project project) {
        project.plugins.hasPlugin("java")
    }

    static <T extends Project> boolean hasGradlePluginPlugin(T project) {
        project.plugins.hasPlugin("java-gradle-plugin")
    }

    static boolean rootPluginHasDependencyManagementPlugin(Project project) {
        project.rootProject != project &&
                project.rootProject.plugins.hasPlugin("io.spring.dependency-management")
    }

    static boolean hasReleasePlugin(Project project) {
        project.plugins.hasPlugin("net.researchgate.release")
    }
}
