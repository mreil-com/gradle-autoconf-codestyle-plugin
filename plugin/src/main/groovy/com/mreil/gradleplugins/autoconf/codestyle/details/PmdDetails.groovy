package com.mreil.gradleplugins.autoconf.codestyle.details

import com.mreil.gradleplugins.autoconf.codestyle.CodestyleExtension

class PmdDetails {
    boolean enabled = true
    String toolVersion = "6.8.0"
    String downloadUrl = "https://bitbucket.org/mreil-com/codestyle/raw/HEAD/pmd/rules.xml"
    File rulesFile
    Boolean ignoreFailures

    private CodestyleExtension extension

    PmdDetails(CodestyleExtension extension) {
        this.extension = extension
        rulesFile = new File(extension.configDir, "pmd/rules.xml")
    }

    boolean getIgnoreFailures() {
        return ignoreFailures == null ? extension.ignoreFailures : ignoreFailures
    }
}
