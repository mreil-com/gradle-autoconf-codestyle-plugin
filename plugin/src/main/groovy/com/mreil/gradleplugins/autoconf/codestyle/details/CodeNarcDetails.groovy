package com.mreil.gradleplugins.autoconf.codestyle.details

import com.mreil.gradleplugins.autoconf.codestyle.CodestyleExtension

class CodeNarcDetails {
    boolean enabled = true
    String toolVersion = "1.2.1"
    String downloadUrl = "https://bitbucket.org/mreil-com/codestyle/raw/HEAD/codenarc/codenarc.xml"
    File configFile
    Boolean ignoreFailures

    private CodestyleExtension extension

    CodeNarcDetails(CodestyleExtension extension) {
        this.extension = extension
        configFile = new File(extension.configDir, "codenarc/codenarc.xml")
    }

    boolean getIgnoreFailures() {
        return ignoreFailures == null ? extension.ignoreFailures : ignoreFailures
    }
}
