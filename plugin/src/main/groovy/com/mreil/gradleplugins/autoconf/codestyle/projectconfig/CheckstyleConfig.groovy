package com.mreil.gradleplugins.autoconf.codestyle.projectconfig

import com.mreil.gradleplugins.autoconf.codestyle.OptionalDownloadTask
import com.mreil.gradleplugins.autoconf.codestyle.details.CheckstyleDetails
import org.gradle.api.Project
import org.gradle.api.plugins.quality.Checkstyle

/**
 * Configure checkstyle for java projects.
 * Executed in the afterEvaluate phase.
 */
class CheckstyleConfig extends ExtensionAwareConfig {
    @Override
    void apply(Project project) {
        project.afterEvaluate {
            applyAfterEvaluate(project)
        }
    }

    private static void applyAfterEvaluate(Project project) {
        CheckstyleDetails extension = getExtension(project).checkstyle
        configurePlugin(project, extension)
        configureDownloadTask(project, extension)
    }

    private static void configurePlugin(Project project,
                                        CheckstyleDetails checkstyleDetails) {

        project.with {
            apply plugin: "checkstyle"

            dependencies {
                checkstyle "com.puppycrawl.tools:checkstyle:${checkstyleDetails.toolVersion}"
            }

            checkstyle {
                configDirectory.set checkstyleDetails.configDir
                ignoreFailures = checkstyleDetails.ignoreFailures
                maxWarnings = 0
            }

            tasks.withType(Checkstyle) {
                reports {
                    xml.enabled false
                    html.enabled true
                }
            }
        }
    }

    private static configureDownloadTask(Project project,
                                         CheckstyleDetails checkstyle) {
        def directory = project.checkstyle.configDirectory
        def download = project.tasks.create("downloadCheckstyleConfig",
                OptionalDownloadTask,
                directory.file("checkstyle.xml").get().asFile,
                checkstyle.downloadUrl)

        project.tasks.findByName("checkstyleMain")?.dependsOn download
        project.tasks.findByName("checkstyleTest")?.dependsOn download
    }

}
