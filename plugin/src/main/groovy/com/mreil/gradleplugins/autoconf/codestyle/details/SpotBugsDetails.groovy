package com.mreil.gradleplugins.autoconf.codestyle.details

import com.mreil.gradleplugins.autoconf.codestyle.CodestyleExtension

class SpotBugsDetails {
    boolean enabled = true
    String toolVersion = "4.0.2"
    String downloadUrl = "https://bitbucket.org/mreil-com/codestyle/raw/HEAD/spotbugs/excludeFilter.xml"
    File excludeFilter
    Boolean ignoreFailures

    private CodestyleExtension extension

    SpotBugsDetails(CodestyleExtension extension) {
        this.extension = extension
        excludeFilter = new File(extension.configDir, "spotbugs/excludeFilter.xml")
    }

    boolean getIgnoreFailures() {
        return ignoreFailures == null ? extension.ignoreFailures : ignoreFailures
    }
}
