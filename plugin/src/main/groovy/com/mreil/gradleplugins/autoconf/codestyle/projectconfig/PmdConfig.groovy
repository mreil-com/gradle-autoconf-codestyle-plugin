package com.mreil.gradleplugins.autoconf.codestyle.projectconfig

import com.mreil.gradleplugins.autoconf.codestyle.OptionalDownloadTask
import com.mreil.gradleplugins.autoconf.codestyle.details.PmdDetails
import org.gradle.api.Project
import org.gradle.api.plugins.quality.Pmd

/**
 * Configure PMD for java projects.
 * Executed in the afterEvaluate phase.
 */
class PmdConfig extends ExtensionAwareConfig {
    @Override
    void apply(Project project) {
        project.afterEvaluate {
            applyAfterEvaluate(project)
        }
    }

    private static void applyAfterEvaluate(Project project) {
        PmdDetails extension = getExtension(project).pmd
        configurePlugin(project, extension)
        configureDownloadTask(project, extension)
    }

    private static void configurePlugin(Project project,
                                        PmdDetails details) {

        project.with {
            apply plugin: "pmd"
            pmd {
                toolVersion = details.toolVersion
                ruleSets = []
                ruleSetFiles = files(details.rulesFile)
                consoleOutput = false
                ignoreFailures = details.ignoreFailures
            }

            tasks.withType(Pmd) {
                reports {
                    xml.setEnabled false
                    html.setEnabled true
                }
            }
        }
    }

    private static configureDownloadTask(Project project,
                                         PmdDetails details) {
        def download = project.tasks.create("downloadPmdConfig",
                OptionalDownloadTask,
                details.rulesFile,
                details.downloadUrl)

        project.tasks.findByName("pmdMain")?.dependsOn download
        project.tasks.findByName("pmdTest")?.dependsOn download
    }

}
