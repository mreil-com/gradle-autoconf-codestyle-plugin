package com.mreil.gradleplugins.autoconf.codestyle.projectconfig

import com.mreil.gradleplugins.autoconf.codestyle.OptionalDownloadTask
import com.mreil.gradleplugins.autoconf.codestyle.details.CodeNarcDetails
import org.gradle.api.Project
import org.gradle.api.plugins.GroovyBasePlugin
import org.gradle.api.plugins.quality.CodeNarc

/**
 * Configure codenarc for groovy projects.
 * Executed in the afterEvaluate phase.
 */
class CodenarcConfig extends ExtensionAwareConfig {
    @Override
    void apply(Project project) {
        project.afterEvaluate {
            applyAfterEvaluate(project)
        }
    }

    private static void applyAfterEvaluate(Project project) {
        if (!project.plugins.hasPlugin(GroovyBasePlugin)) {
            return
        }

        CodeNarcDetails extension = getExtension(project).codenarc
        configurePlugin(project, extension)
        configureDownloadTask(project, extension)
    }

    private static void configurePlugin(Project project,
                                        CodeNarcDetails details) {

        project.with {
            apply plugin: "codenarc"

            codenarc {
                toolVersion = details.toolVersion
                configFile = details.configFile
                ignoreFailures = details.ignoreFailures
            }

            tasks.withType(CodeNarc) {
                reports {
                    xml.enabled false
                    html.enabled true
                }
            }
        }
    }

    private static configureDownloadTask(Project project,
                                         CodeNarcDetails details) {
        def download = project.tasks.create("downloadCodenarcConfig",
                OptionalDownloadTask,
                details.configFile,
                details.downloadUrl)

        project.tasks.findByName("codenarcMain")?.dependsOn download
        project.tasks.findByName("codenarcTest")?.dependsOn download
    }

}
