package com.mreil.gradleplugins.autoconf.codestyle.projectconfig

import com.github.spotbugs.snom.SpotBugsTask
import com.mreil.gradleplugins.autoconf.codestyle.OptionalDownloadTask
import com.mreil.gradleplugins.autoconf.codestyle.details.SpotBugsDetails
import org.gradle.api.Project

/**
 * Configure spotbugs for java projects.
 * Executed in the afterEvaluate phase.
 */
class SpotbugsConfig extends ExtensionAwareConfig {
    @Override
    void apply(Project project) {
        project.afterEvaluate {
            applyAfterEvaluate(project)
        }
    }

    private static void applyAfterEvaluate(Project project) {
        SpotBugsDetails extension = getExtension(project).spotbugs
        configurePlugin(project, extension)
        configureDownloadTask(project, extension)
    }

    private static void configurePlugin(Project project,
                                        SpotBugsDetails spotBugs) {

        project.with {
            apply plugin: "com.github.spotbugs"
            spotbugs {
                toolVersion = spotBugs.toolVersion
                excludeFilter = spotBugs.excludeFilter
                ignoreFailures = spotBugs.ignoreFailures
            }

            tasks.withType(SpotBugsTask) {
                reports {
                    xml.setEnabled false
                    html.setEnabled true
                }
            }

            dependencies {
                spotbugsPlugins 'com.h3xstream.findsecbugs:findsecbugs-plugin:1.7.1'
            }
        }
    }

    private static configureDownloadTask(Project project,
                                         SpotBugsDetails spotBugs) {
        def download = project.tasks.create("downloadSpotbugsConfig",
                OptionalDownloadTask,
                project.spotbugs.excludeFilter.asFile.get(),
                spotBugs.downloadUrl)

        project.tasks.findByName("spotbugsMain")?.dependsOn download
        project.tasks.findByName("spotbugsTest")?.dependsOn download
    }

}
