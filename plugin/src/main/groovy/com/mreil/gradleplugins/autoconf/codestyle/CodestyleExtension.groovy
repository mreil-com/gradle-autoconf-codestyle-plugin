package com.mreil.gradleplugins.autoconf.codestyle

import com.mreil.gradleplugins.autoconf.codestyle.details.CheckstyleDetails
import com.mreil.gradleplugins.autoconf.codestyle.details.CodeNarcDetails
import com.mreil.gradleplugins.autoconf.codestyle.details.PmdDetails
import com.mreil.gradleplugins.autoconf.codestyle.details.SpotBugsDetails

interface CodestyleExtension {
    public static final String NAME = "codestyle"

    File getConfigDir()

    CheckstyleDetails getCheckstyle()
    void checkstyle(Closure details)

    SpotBugsDetails getSpotbugs()
    void spotbugs(Closure details)

    CodeNarcDetails getCodenarc()
    void codenarc(Closure details)

    PmdDetails getPmd()
    void pmd(Closure details)

    boolean getIgnoreFailures()

    void setIgnoreFailures(boolean val)
}
