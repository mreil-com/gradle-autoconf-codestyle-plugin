package com.mreil.gradleplugins.autoconf.codestyle

import com.mreil.gradleplugins.autoconf.codestyle.projectconfig.CheckstyleConfig
import com.mreil.gradleplugins.autoconf.codestyle.projectconfig.CodenarcConfig
import com.mreil.gradleplugins.autoconf.codestyle.projectconfig.PmdConfig
import com.mreil.gradleplugins.autoconf.codestyle.projectconfig.SpotbugsConfig
import org.gradle.api.Plugin
import org.gradle.api.Project

class AutoconfCodestylePlugin implements Plugin<Project> {
    @Override
    void apply(Project project) {
        validateProject(project)

        // create extension if it doesn't exist
        if (project.extensions.findByName(CodestyleExtension.NAME) == null) {
            project.extensions.create(CodestyleExtension.NAME,
                    AutoconfCodestyleExtension,
                    project)
        }

        // configure the project and its subprojects
        ([project] + project.subprojects).forEach {
            new SpotbugsConfig().apply(it)
            new CheckstyleConfig().apply(it)
            new PmdConfig().apply(it)
            new CodenarcConfig().apply(it)
        }
    }

    static void validateProject(Project project) {
        def version = project.getGradle().gradleVersion
        if (version < "6") {
            def msg = String.format("This plugin requires at least Gradle version 6.0. Version detected: %s", version)
            throw new IllegalStateException(msg)
        }
    }
}
