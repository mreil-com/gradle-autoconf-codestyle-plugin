package com.mreil.gradleplugins.autoconf.codestyle

import com.mreil.gradleplugins.autoconf.codestyle.details.CheckstyleDetails
import com.mreil.gradleplugins.autoconf.codestyle.details.CodeNarcDetails
import com.mreil.gradleplugins.autoconf.codestyle.details.PmdDetails
import com.mreil.gradleplugins.autoconf.codestyle.details.SpotBugsDetails
import org.apache.commons.lang3.ObjectUtils
import org.gradle.api.Project

class AutoconfCodestyleExtension implements CodestyleExtension {
    Project project
    File configDir
    boolean ignoreFailures = false

    CheckstyleDetails checkstyle
    SpotBugsDetails spotbugs
    CodeNarcDetails codenarc
    PmdDetails pmd

    AutoconfCodestyleExtension(Project project) {
        this.project = project
        this.configDir = ObjectUtils.defaultIfNull(project.getParent(), project).file("config/codestyle")

        checkstyle = new CheckstyleDetails(this)
        spotbugs = new SpotBugsDetails(this)
        codenarc = new CodeNarcDetails(this)
        pmd = new PmdDetails(this)
    }

    @Override
    void checkstyle(Closure details) {
        details.delegate = checkstyle
        details.resolveStrategy = Closure.DELEGATE_FIRST
        details()
    }

    @Override
    void spotbugs(Closure details) {
        details.delegate = spotbugs
        details.resolveStrategy = Closure.DELEGATE_FIRST
        details()
    }

    @Override
    void codenarc(Closure details) {
        details.delegate = codenarc
        details.resolveStrategy = Closure.DELEGATE_FIRST
        details()
    }

    @Override
    void pmd(Closure details) {
        details.delegate = pmd
        details.resolveStrategy = Closure.DELEGATE_FIRST
        details()
    }

    @Override
    boolean getIgnoreFailures() {
        return ignoreFailures
    }

    @Override
    void setIgnoreFailures(boolean val) {
        ignoreFailures = val
    }
}
