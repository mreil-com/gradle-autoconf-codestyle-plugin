package com.mreil.gradleplugins.autoconf.codestyle.details

import com.mreil.gradleplugins.autoconf.codestyle.CodestyleExtension

class CheckstyleDetails {
    boolean enabled = true
    String toolVersion = "8.13"
    String downloadUrl = "https://bitbucket.org/mreil-com/codestyle/raw/HEAD/checkstyle/checkstyle.xml"
    File configDir
    Boolean ignoreFailures

    private CodestyleExtension extension

    CheckstyleDetails(CodestyleExtension extension) {
        this.extension = extension
        configDir = new File(extension.configDir, "checkstyle")
    }

    void setConfigDir(String dir) {
        setConfigDir(new File(dir))
    }

    void setConfigDir(File file) {
        if(!file.isAbsolute()) {
            file = extension.configDir.toPath().resolve(file.toPath()).toFile()
        }
        configDir = file
    }

    boolean getIgnoreFailures() {
        return ignoreFailures == null ? extension.ignoreFailures : ignoreFailures
    }
}
