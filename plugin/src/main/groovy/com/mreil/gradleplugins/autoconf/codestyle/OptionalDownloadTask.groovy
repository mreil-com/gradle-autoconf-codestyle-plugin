package com.mreil.gradleplugins.autoconf.codestyle

import com.mreil.gradleplugins.autoconf.ConfigDownloader
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import javax.inject.Inject

class OptionalDownloadTask extends DefaultTask {
    @OutputFile
    File file

    @Input
    String url

    @Inject
    OptionalDownloadTask(File file,
                         String url) {
        this.file = file
        this.url = url
    }

    @TaskAction
    def execute() {
        def dl = new ConfigDownloader(url,
                file,
                project.logger)
        dl.download()
    }
}
