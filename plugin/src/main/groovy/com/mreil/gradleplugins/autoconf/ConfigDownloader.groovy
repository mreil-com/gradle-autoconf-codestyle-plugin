package com.mreil.gradleplugins.autoconf

import org.slf4j.Logger

/**
 * Download a file to a target location if it doesn't exist.
 */
class ConfigDownloader {
    private final String url
    private final File target
    private final Logger logger

    ConfigDownloader(String url,
                     File target,
                     Logger logger) {
        this.url = url
        this.target = target
        this.logger = logger
    }

    /**
     * Download the file if the target doesn't exist yet.
     * @return true if the file was downloaded, false if it already existed
     */
    boolean download() {
        if (!target.exists()) {
            logger.info("Local file not found. Downloading from: ${url}")

            target.parentFile.mkdirs()
            target.withOutputStream { out ->
                out << new URL(url).openStream()
            }
            return true
        } else {
            logger.info("Local file ${target} exists. Skipping download.")
            return false
        }
    }

}
