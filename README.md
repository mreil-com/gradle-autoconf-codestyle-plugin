# gradle-autoconf-codestyle-plugin

[![Maven version](https://img.shields.io/maven-metadata/v/https/plugins.gradle.org/m2/com/mreil/autoconf-codestyle/com.mreil.autoconf-codestyle.gradle.plugin/maven-metadata.xml.svg)](https://plugins.gradle.org/plugin/com.mreil.autoconf)
[![Build status](https://img.shields.io/bitbucket/pipelines/mreil-com/gradle-autoconf-codestyle-plugin.svg)](https://bitbucket.org/mreil-com/gradle-autoconf-codestyle-plugin)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

> Automatically configure codestyle tools for a Gradle project

## Install

Install the plugin as follows.

```
#!groovy

plugins {
    id "com.mreil.autoconf-codestyle" version "0.0.3"
}

```


## Usage

### Configuration

The plugin can be configured as follows:

```
#!groovy

codestyle {
    ignoreFailures = false      // fail build after warnings

    checkstyle {
        toolVersion = "8.21"
        ignoreFailures = false
    }

    pmd {
        toolVersion = "6.12.0"
        ignoreFailures =false
    }

    spotbugs {
        toolVersion = "3.1.12"
        ignoreFailures = false
    }

    codenarc {
        toolVersion = "1.4"
        ignoreFailures = false
    }
}
```


### Automatically applied configuration

[Checkstyle][checkstyle], [SpotBugs][spotbugs], [CodeNarc][codenarc] and [PMD][pmd] will be 
configured and reports will be generated during the `check` phase.


### Try it

Check out the repository and build and publish to your local maven repo by running

```bash
./gradlew -xcheck publish
```

Then, in the `sample-project` directory, run

```bash
./gradlew check
```


## Building

Gradle dependency locking is enabled.
Use

```bash
./gradlew resolveAndLockAll
```

to check and

```bash
./gradlew resolveAndLockAll --write-locks
```

to update dependencies.


## Changes

See [CHANGELOG.md][changelog] for changes.


## Maintainer

[Markus Reil](https://bitbucket.org/mreil-com/)


## Contribute

Please raise issues [here](../../issues?status=new&status=open).

Feel free to address existing issues and raise a [pull request](../../pull-requests).


## License

[MIT][license]


[changelog]: CHANGELOG.md
[license]: LICENSE
[checkstyle]: http://checkstyle.sourceforge.net/
[spotbugs]: https://spotbugs.github.io/
[codenarc]: http://codenarc.sourceforge.net/
[pmd]: https://pmd.github.io/
